#define _CRT_SECURE_NO_WARNINGS 
#include <stdio.h> 
#include <stdlib.h> 
#define N 20 

typedef struct spisok {
	struct spisok *next;
	char simvol;
}spisok;

void free_sp(spisok *some) {
	spisok *support;
	while (some != NULL) {
		support = some;
		some = some->next;
		free(support);
	}
}

int main(int argc, char *argv[]) {
	if (argc != 2) {
		printf("error command arguments");
		return 1;
	}
	spisok *skoby = NULL;
	spisok *support = NULL;
	char dop[256] = { 0 };
	char dop_two[256] = { 0 };
	dop['}'] = '{';
	dop['>'] = '<';
	dop[']'] = '[';
	dop[')'] = '(';
	dop_two['{'] = '{';
	dop_two['<'] = '<';
	dop_two['['] = ']';
	dop_two['('] = '(';
	int i;
	char mass[N] = { 0 };
	FILE* myfile = NULL;
	myfile = fopen(argv[1], "rb");
	if (myfile == NULL) {
		printf("error file");
		return 1;
	}
	while (!feof(myfile)) {
		fgets(mass, N, myfile);
		for (i = 0; i < N && mass[i] != 0; ++i) {
			if (dop_two[mass[i]] != 0) {
				support = malloc(sizeof(spisok));
				support->simvol = mass[i];
				support->next = skoby;
				skoby = support;
			}
			if (dop[mass[i]] != 0) {

				if (skoby == NULL) {
					printf("error");
					free_sp(skoby);
					fclose(myfile);
					return 1;
				}
				else {
					if (skoby->simvol != dop[mass[i]]) {
						printf("error");
						free_sp(skoby);
						fclose(myfile);
						return 1;
					}
					else {
						support = skoby->next;
						free(skoby);
						skoby = support;
						break;
					}
					if (support == NULL) {
						printf("error");
						free_sp(skoby);
						fclose(myfile);
						return 1;
					}
				}
			}
		}
		for (i = 0; i < N; ++i) {
			mass[i] = 0;
		}
	}
	fclose(myfile);
	free_sp(skoby);
	return 0;
}