#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include ""../libriary/mylib.h"

void BFS(graph obhod, int start, int **metka) {
	int amount = zapros_size(obhod);
	int *mass = (int*)malloc(sizeof(int)*amount);
	int i;
	int j = 0;
	int schet = 0;
	j = 0;
	mass[schet] = start;
	while (j != amount - 1) {
		for (i = 0; i < amount; ++i) {
			if (proverka(obhod, mass[schet], i) == 1 && metka[mass[schet]][i] == 0) {
				metka[mass[schet]][i] = 1;
				metka[i][mass[schet]] = 1;
				printf("%i ", i + 1);
				++j;
				mass[j] = i;
			}
		}
		++schet;
	}
	free(mass);
}

void DFS(graph obhod, int start, int **metka) {
	int amount = zapros_size(obhod);
	for (int i = 0; i < amount; ++i) {
		if (proverka(obhod, start, i) == 1 && metka[start][i] == 0) {
			metka[i][start] = 1;
			metka[start][i] = 1;
			printf("%i ", i + 1);
			DFS(obhod, i, metka);
		}
	}
}

int main(int argc, char *argv[]) {
	if (argc != 2) {
		printf("error command arguments");
		return 0;
	}
	FILE *myfile = NULL;
	int start;
	graph obhod = NULL;
	int choise = 0;
	int i;
	obhod = read_file(argv[1]);
	printf("If you want to use BFS, then print '1'.\nIf you want to use DFS, then print '2'\n");
	scanf("%i", &choise);
	printf("\nEnter start graph\n");
	scanf("%i", &start); // ������� ������ ���� ������ ��� �������?
	--start;
	int amount = zapros_size(obhod);
	int **metka = (int**)malloc(sizeof(int)*amount);
	for (i = 0; i < amount; ++i) {
		metka[i] = (int*)malloc(sizeof(int)*amount);
		for (int j = 0; j < amount; ++j) {
			metka[i][j] = 0;
		}
	}
	if (choise == 1) {
		BFS(obhod, start, metka);
	}
	if (choise == 2) {
		DFS(obhod, start, metka);
	}
	for (i = 0; i < amount; ++i) {
		free(metka[i]);
	}
	free(metka);
	delete_graph(obhod);
	return 0;
}