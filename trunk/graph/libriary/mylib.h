﻿#ifndef mylib
#define mylib
typedef void* graph;
graph delete_graph(graph support);
void new_rebro(graph support, int start, int end, int vsvesh);
void delete_rebro(graph support, int start, int end);
void save_graph(graph support);
graph copy_graph(graph support);
int proverka(graph support, int start, int end);
int ves(graph support, int start, int end);
int zapros_size(graph support);
graph read_file(char *name);
graph create_graph(int amount, int orien, int vzvesh, int type);
#endif
/* 1 - size
2.1 - ориентированный
2.2 - неориентированный
3.1 - взвешенный
3.2 невзвешенный
4.1 матрица смежности
4.2 массив списков
*/