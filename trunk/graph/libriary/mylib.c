﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include "mylib.h"
/* 1 - size
   2.1 - ориентированный
   2.2 - неориентированный
   3.1 - взвешенный
   3.2 невзвешенный
   4.1 матрица смежности
   4.2 массив списков
*/

typedef struct graph_spisok{
	struct graph_spisok *next;
	int connect;
	int ves;
}graph_spisok;

typedef struct graph_mat {
	int **matrix;
	graph_spisok **mass;
	int amount;
	int orien;
	int vzvesh;
	int type;
}graph_mat;

typedef struct graph_live {
	struct graph_live *next;
	struct graph_mat *adress;
}graph_live;

graph_live *live = NULL;

char proverka_graph(graph support) {
	graph_live *dop_help = live;
	char exzistence = 0;
	if (support == NULL) {
		return exzistence;
	}
	while (dop_help != NULL) {
		if (dop_help->adress == support) {
			exzistence = 1;
			break;
		}
		dop_help = dop_help->next;
	}
	return exzistence;
}

graph delete_graph(graph support) {
	if (proverka_graph(support) == 0) {
		return;
	}
	graph_mat *basic = support;
	int amount = basic->amount;
	if (basic->type == 1) {
		for (int i = 0; i < amount; ++i) {
			free(basic->matrix[i]);
		}
		free(basic->matrix);
	}
	else {
		graph_spisok *support = NULL;
		for (int i = 0; i < amount; ++i) {
			if (basic->mass[i] != NULL) {
				while (basic->mass[i] != NULL) {
					support = basic->mass[i]->next;
					free(basic->mass[i]);
					basic->mass[i] = support;
				}
			}
		}
		free(basic->mass);
	}
	free(basic);
	graph_live *dop_help_one = live;
	graph_live *dop_help_two = NULL;
	while (dop_help_one != NULL) {
		if (dop_help_one->adress == support) {
			graph_live *dop_help_three = dop_help_one->next;
			if (dop_help_two == NULL) {
				live = dop_help_three;
			}
			else {
				dop_help_two->next = dop_help_three;
			}
			free(dop_help_one);
			break;
		}
		dop_help_two = dop_help_one;
		dop_help_one = dop_help_one->next;
	}
	return NULL;
}

void new_rebro(graph support, int start, int end, int vzvesh) {
	graph_mat *basic = support;
	if (proverka_graph(support) == 0 || start < 0 || start >= basic->amount || end < 0 || end >= basic->amount || vzvesh == 0) {
		return;
	}
	if (basic->type == 1) {
		if (basic->vzvesh == 1) {
			basic->matrix[start][end] = vzvesh;
		}
		else {
			if (vzvesh == 1) {
				basic->matrix[start][end] = vzvesh;
			}
		}
	}
	else {
		graph_spisok *support = NULL;
		support = basic->mass[start];
		while (support != NULL) {
			if (support->connect == end) {
				support->ves = vzvesh;
				return;
			}
			support = support->next;
		}
		support = basic->mass[start];
		basic->mass[start] = malloc(sizeof(graph_spisok));
		basic->mass[start]->connect = end;
		basic->mass[start]->next = support;
		if (basic->vzvesh == 1) {
			basic->mass[start]->ves = vzvesh;
		}
		else {
			if (vzvesh == 1) {
				basic->mass[start]->ves = vzvesh;
			}
		}
	}
}

void delete_rebro(graph support, int start, int end) {
	graph_mat *basic = support;
	if (proverka_graph(support) == 0 || start < 0 || start >= basic->amount || end < 0 || end >= basic->amount) {
		return;
	}
	if (basic->type == 1) {
		basic->matrix[start][end] = 0;
	}
	else {
		graph_spisok *support = basic->mass[start];
		graph_spisok *support_two = NULL;
		while (support != NULL) {
			if (support->connect == end) {
				graph_spisok *support_three = support->next;
				if (support_two == NULL) {
					basic->mass[start] = support_three;
				}
				else {
					support_two->next = support_three;
				}
				free(support);
				break;
			}
			support_two = support;
			support = support->next;
		}
	}
}

int proverka(graph support, int start, int end) {
	graph_mat *basic = support;
	if (proverka_graph(support) == 0 || start < 0 || start >= basic->amount || end < 0 || end >= basic->amount) {
		return 0;
	}
	if (basic->type == 1) {
		if (basic->matrix[start][end] == 0) {
			return 0;
		}
		else {
			return 1;
		}
	}
	else {
		graph_spisok *support = basic->mass[start];
		while (support != NULL) {
			if (support->connect == end) {
				return 1;
			}
			support = support->next;
		}
		return 0;
	}
}

int ves(graph support, int start, int end){
	graph_mat *basic = support;
	if (proverka_graph(support) == 0 || start < 0 || start >= basic->amount || end < 0 || end >= basic->amount) {
		return 0;
	}
	if (basic->type == 1) {
		return basic->matrix[start][end];
	}
	else {
		graph_spisok *support = basic->mass[start];
		while (support != NULL) {
			if (support->connect == end) {
				return support->ves;
			}
			support = support->next;
		}
	}
	return 0;
}

int zapros_size(graph support) {
	if (proverka_graph(support) == 0) {
		return 0;
	}
	graph_mat *basic = support;
    return basic->amount;
}

graph copy_graph(graph support) {
	if (proverka_graph(support) == 0) {
		return NULL;
	}
	graph *basic = NULL;
	graph_mat *old = support;
	basic = create_graph(old->amount, old->orien, old->vzvesh, old->type);
	for (int i = 0; i < old->amount; ++i) {
		for (int j = 0; j < old->amount; ++j) {
			if (proverka(support, i, j)) {
				new_rebro(basic, i, j, ves(support, i, j));
			}
		}
	}
	return basic;
}

void save_graph(graph support){
	if (proverka_graph(support) == 0) {
		return;
	}
	graph_mat *basic = support;
	char name[] = { "copy_graph.txt" };// сохраняет в папку с исполняемым файлом.
	int help;
	FILE* myfile = fopen(name, "wb");
	fprintf(myfile, "%i %i %i %i\r\n", basic->amount, basic->orien, basic->vzvesh, basic->type);
	for (int i = 0; i < basic->amount; ++i) {
		for (int j = 0; j < basic->amount; ++j) {
			if (proverka(support, i, j) == 1) {
				if (basic->vzvesh == 1) {
					help = ves(support, i, j);
					fprintf(myfile, "%i %i %i\r\n", i, j, help);
				}
				else {
					fprintf(myfile, "%i %i %i\r\n", i, j, 1);
				}
			}
		}
	}
	fclose(myfile);
}

graph create_graph(int amount, int orien, int vzvesh, int type) {
	int i;
	if (amount < 1 || (orien != 1 && orien != 2)
		|| (vzvesh != 1 && vzvesh != 2) || (type != 1 && type != 2)) {
		return NULL;
	}
	graph_mat *basic = malloc(sizeof(graph_mat));
	basic->matrix = NULL;
	basic->mass = NULL;
	basic->amount = amount;
	basic->orien = orien;
	basic->vzvesh = vzvesh;
	basic->type = type;
	graph_live *dop_help = live;
	live = malloc(sizeof(graph_live));
	live->adress = basic;
	live->next = dop_help;
	if (basic->type == 1) {
		basic->matrix = (int**)calloc(basic->amount,sizeof(int*));
		for (i = 0; i < basic->amount; ++i) {
			basic->matrix[i] = calloc(basic->amount,sizeof(int));
		}
	}
	else {
		basic->mass = (graph_spisok**)malloc(sizeof(graph_spisok*)*basic->amount);
		for (i = 0; i < basic->amount; ++i) {
			basic->mass[i] = NULL;
		}
	}
	return basic;
}

graph read_file(char *name) {
	if (name == NULL) {
		return NULL;
	}
	FILE* myfile;
	int i;
	graph_mat *basic = NULL;
	int temp_one = 0;
	int temp_two = 0;
	int temp_three = 0;
	int amount = 0;
	int orien = 0;
	int vzvesh = 0;
	int type = 0;
	int schit = 0;
	myfile = fopen(name, "rb");
	if (myfile == NULL) {
		return NULL;
	}
	schit = fscanf(myfile, "%i%i%i%i", &amount, &orien, &vzvesh, &type);
	if (schit != 4) {
		fclose(myfile);
		return NULL;
	}
 	basic = create_graph(amount, orien, vzvesh, type);
	if (basic == NULL) {
		fclose(myfile);
		return NULL;
	}
	while (!(feof(myfile))) {
			schit = fscanf(myfile, "%i%i%i", &temp_one, &temp_two, &temp_three);
			if (temp_one < 1 || temp_one > basic->amount || temp_two < 1 || temp_two > basic->amount) {
				fclose(myfile);
				delete_graph(basic);
				return NULL;
			}
			else {
				if (schit == 3) {
					--temp_one;
					--temp_two;
					if (basic->orien == 1) {
						new_rebro(basic, temp_one, temp_two, temp_three);
					}
					else {
						new_rebro(basic, temp_two, temp_one, temp_three);
						new_rebro(basic, temp_one, temp_two, temp_three);
					}
				}
			}
	}
	fclose(myfile);
	return basic;
}