﻿#include <stdio.h>
#include "../libriary/mylib.h"

int main() {
	graph kop = copy_graph(NULL);//неправильный указатель
	graph some = create_graph(4, 1, 3, 1);//некорректные данные
	some = create_graph(4, 1, 2, 2);
	delete_graph(some);
	delete_graph(some);//двойное удаление
	some = create_graph(4, 1, 1, 2);
	int amount = zapros_size(some);
	for (int i = 0; i < amount; ++i) {
		for (int j = 0; j < amount; ++j) {
			if (proverka(some, i, j)) {
				printf("%i %i\n", i, j);
			}
		}
	}
	new_rebro(some, 1, 2, 3);
	new_rebro(some, 2, 3, 4);
	new_rebro(some, 0, 2, 5);
	new_rebro(some, 0, 2, 0);// некорректный вес
	for (int i = 0; i < amount; ++i) {
		for (int j = 0; j < amount; ++j) {
			if (proverka(some, i, j)) {
				printf("%i %i\n", i, j);
			}
		}
	}
	printf("\n\n");
	delete_rebro(some, 1, 2);
	delete_rebro(some, 0, 2);
	for (int i = 0; i < amount; ++i) {
		for (int j = 0; j < amount; ++j) {
			if (proverka(some, i, j)) {
				printf("%i %i\n", i, j);
			}
		}
	}
	printf("\n\n");
	new_rebro(some, 1, 2, 3);
	new_rebro(some, 0, 2, 5);
	graph *support = copy_graph(some);
	save_graph(support);
	delete_graph(support);
	support = read_file("copy_graph.txt");
	for (int i = 0; i < amount; ++i) {
		for (int j = 0; j < amount; ++j) {
			if (proverka(some, i, j)) {
				printf("%i %i\n", i, j);
			}
		}
	}
	delete_graph(support);
	delete_graph(some);
	return 0;
}