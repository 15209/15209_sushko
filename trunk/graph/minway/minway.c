#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include "../libriary/mylib.h"

int witcher(graph way, int start, int end) {
	int amount = zapros_size(way);
	int i;
	int temp;
	int result;
	int **metka = (int**)malloc(sizeof(int)*amount);
	for (i = 0; i < amount; ++i) {
		metka[i] = (int*)malloc(sizeof(int)*amount);
		for (int j = 0; j < amount; ++j) {
			metka[i][j] = 0;
		}
	}
	int *verh = malloc(sizeof(int)*amount);
	for (i = 0; i < amount; ++i) {
		verh[i] = 1000000;
	}
	verh[start] = 0;
	int *stek = (int*)malloc(sizeof(int)*amount);
	for (i = 0; i < amount; ++i) {
		stek[i] = -1;
	}
	stek[0] = start;
	int schet = 0;
	int dop_numb;
	while (stek[schet] != -1 && schet < amount) {
		for (i = 0; i < amount; ++i) {
			temp = ves(way, stek[schet], i);
			dop_numb = 1;
			if (temp != 0 && metka[stek[schet]][i] != 1 && verh[i] > verh[stek[schet]]+temp && metka[i][i] != 1) {
				verh[i] = verh[stek[schet]] + temp;
				metka[stek[schet]][i] = 1;
				stek[schet + dop_numb] = i;
				++dop_numb;
				metka[i][i] = 1;
			}
		}
		metka[stek[schet]][stek[schet]] = 1;
		++schet;
	}
	result = verh[end];
	for (i = 0; i < amount; ++i) {
		free(metka[i]);
	}
	free(metka);
	free(stek);
	free(verh);
	return result;
}

int main(int argc, char *argv[]) {
	if (argc != 2) {
		printf("error command arguments");
		return 0;
	}
	FILE *myfile = NULL;
	graph way = NULL;
	way = read_file(argv[1]);
	int start;
	int end;
	int rezult;
	int end_program = 1;
	while (1) {
		printf("write start and end\n");
		scanf("%i%i", &start, &end);
		--start;
		--end;
		rezult = witcher(way, start, end);
		if (rezult == 1000000) {
			printf("Not way");
		}
		else {
			printf("%i ", rezult);
		}
		printf("\nDo you would like close programm? if yes> then print '0' ");
		scanf("%i", &end_program);
		if (end_program == 0) {
			break;
		}
	}
	delete_graph(way);
	return 0;
}