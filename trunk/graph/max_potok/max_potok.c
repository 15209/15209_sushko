﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include "../libriary/mylib.h"

typedef struct spisok {
	int kyda;
	struct spisok *next;
}spisok;

int amount; 
int max = 0;
int now_min = 999999999;
spisok *way = NULL;
int step = 1;
int *metka = NULL;

int minim(int a, int b) {
	if (a < b) {
		return a;
	}
	else {
		return b;
	}
}

void pot_ok(graph potok, int istok, int stok) {
	int amount = zapros_size(potok);
	int vs_reb;
	int help;
	spisok *support = way;
		if (stok != istok) {
			for (int i = 0; i < amount; ++i) {
				vs_reb = ves(potok, istok, i);
				if (metka[i] == 0 && vs_reb > 0 && step == 1) {
					metka[istok] = 1;
					spisok *support = way;
					way = malloc(sizeof(spisok));
					way->next = support;
					way->kyda = i;
					now_min = minim(vs_reb, now_min);
					pot_ok(potok, i, stok);
					metka[istok] = 0;
				}
			}
		}
		else {
			support = way;
			while (support != NULL) {
				if (support->next != NULL) {
					help = support->next->kyda;
				}
				else {
					help = 0;
				}
				if (now_min == ves(potok, help, support->kyda)) {
					delete_rebro(potok, help, support->kyda);
				}
				else {
					new_rebro(potok, help, support->kyda, ves(potok, help, support->kyda) - now_min);
				}
				new_rebro(potok, support->kyda, help, ves(potok, support->kyda, help) + now_min);
				support = support->next;
			}
			max += now_min;
			step = 2;
		}
		support = way;
		if (way != NULL) {
			way = way->next;
			free(support);
		}
		now_min = 999999999;
		support = way;
		while (support != NULL && step == 1) {
			if (support->next != NULL) {
				help = support->next->kyda;
			}
			else {
				help = 0;
			}
			now_min = minim(ves(potok, help, support->kyda), now_min);
			support = support->next;
		}
}

int poisk(graph potok, int istok, int stok) {
	int old = -1;
	int amount = zapros_size(potok);
	while (old != max) {
		old = max;
		pot_ok(potok, istok, stok);
		step = 1;
		now_min = 9999999999;
	}
}

int main(int argc, char *argv[]) {
	if (argc != 2) {
		printf("error command arguments");
		return 0;
	}
	FILE *myfile = NULL;
	graph potok = NULL;
	potok = read_file(argv[1]);
	int start;
	int end;
	amount = zapros_size(potok);
	while (1) {
		scanf("%i%i", &start, &end);
		if (start == 0 || end == 0) {
			break;
		}
		else {
			if (start == end) {
				printf("0");
			}
			else {
				--start;
				--end;
				metka = calloc(amount, sizeof(int));
				poisk(potok, start, end);
				printf("\n%i", max);
			}
		}
	}
	return 0;
}