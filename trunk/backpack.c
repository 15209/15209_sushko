#include <stdio.h>
#include <stdlib.h>
#define N 5
#define maxmass 25
typedef struct box {
	int mass;
	int price;
	int metka;
}box;

typedef struct spisok{
	int numb;
	struct spisok *ykaz;
}spisok;

spisok now_way = { -1, NULL };
spisok *best_price = NULL;
int best_pay = 0;
int indik = 0;

void free_sp(spisok *some) {
	spisok *support;
		while (some != NULL) {
			support = some;
			some = some->ykaz;
			free(support);
		}
}


void copy() {
	int i = 0;
	spisok *help = now_way.ykaz;
	while (help != NULL) {
		if (i == 0) {
			best_price = malloc(sizeof(spisok));
			best_price->numb = help->numb;
			best_price->ykaz = NULL;
		}
		else {
			spisok *support = NULL;
			support = malloc(sizeof(spisok));
			support->ykaz = best_price;
			support->numb = help->numb;
			best_price = support;
		}
		help = help->ykaz;
		++i;
	}
}

void rek(box *backpack, int nowmass, int nowprice, int i){
	spisok *help = &now_way;
	while (help->ykaz != NULL) {
		help = help->ykaz;
	}
	if (nowmass > maxmass) {
		nowmass -= backpack[help->numb].mass;
		nowprice -= backpack[help->numb].price;
		if (nowprice > best_pay) {
			best_pay = nowprice;
			if (best_price != NULL) {
				free_sp(best_price);
			}
			copy();
		}
		return;
	}
	int j;
	spisok *metka = NULL;
	nowmass+=backpack[i].mass;
	nowprice+=backpack[i].price;
	help->ykaz = malloc(sizeof(spisok));
	metka = help;
	help = help->ykaz;
	help->ykaz = NULL;
	help->numb = i;
	backpack[i].metka = 1;
	++indik;
	for (j = 0; j < N; ++j) {
		if (backpack[j].metka != 1) {
			rek(backpack, nowmass, nowprice, j);
		}
	}
	backpack[i].metka = 0;
	if (indik == N) {
			if (nowprice > best_pay) {
				best_pay = nowprice;
				if (best_price != NULL) {
					free_sp(best_price);
				}
				copy();
			}
			return;
	}
	--indik;
	if (metka != NULL) {
		free(metka->ykaz);
		metka->ykaz = NULL;
	}
}

int main() {
	box backpack[N];
	spisok *help = NULL;
	int nowmass = 0;
	int nowprice = 0;
	backpack[0].mass = 5;
	backpack[0].price = 3;
	backpack[1].mass = 9;
	backpack[1].price = 4;
	backpack[2].mass = 3;
	backpack[2].price = 7;
	backpack[3].mass = 6;
	backpack[3].price = 4;
	backpack[4].mass = 2;
	backpack[4].price = 1;
	for (int i = 0; i < N; ++i) {
		rek(backpack, nowmass, nowprice, i);
	}
	free_sp(now_way.ykaz);
	help = best_price;
	while (help != NULL) {
		printf("%i ", help->numb);
		help=help->ykaz;
	}
	free_sp(best_price);
	printf("\nBest price %i\n", best_pay);
	return 0;
}