#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>

int main() {
	int sdvig = 1023;
	long long int znak;
	long long int expon;
	long long int mantis;
	long long int *bity = NULL;
	long long int mask_mantis = 0xfffffffffffff;
	long long int mask_expon = 0x7ff;
	int max_expon = 0x7ff;
	double number;
	double *bbt=NULL;
	double mantis_now;
	double test;
	int test1;
	while (1) {
		printf("\nWrite number: ");
		scanf("%lf", &number);
		bity = &number;
		znak = *bity >> 63;
		expon = (*bity >> 52) & mask_expon;
		mantis = *bity & mask_mantis;
		if (expon == max_expon && mantis == 0) {
			if (znak == 0) {
				printf("+Infinity");
				return 0;
			}
			else {
				printf("-Infinity");
				return 0;
			}
		}
		if (expon == max_expon && mantis > 0) {
			printf("NaN");
			return 0;
		}
		mantis = mantis | 0x3fe0000000000000;
		bbt = &mantis;
		mantis_now = *bbt;
		expon = expon - sdvig + 1;
		if (znak == 0) {
			printf("\nZnak: +\n");
		}
		else {
			printf("\nZnak: -\n");
		}
		printf("Exponent: %lli\nMantissa: %lf ", expon, mantis_now);
		test = frexp(number, &test1);
		printf("\nWith help flexp: %lf     %i", test, test1);
	}
	return 0;
}
