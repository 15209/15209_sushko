﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "arhivator.h"

typedef struct drevo {
	struct drevo *left;
	struct drevo *right;
	uchar simvol;
	uint chastota;
}drevo;

typedef struct spisok {
	uint chastota;
	struct spisok* next;
	drevo *vetka;
}spisok;

uint kolvo = 0;// количество элементов
uint power = 0;//количество различных символов
shablon *table = NULL;
uint now_power = 0;//для постройки таблицы
uchar code[N] = { 0 };//временное хранение кода символа
uchar *rash = NULL;
uchar size_rash = 0;
uint kolvo_meta = 0;

char* create_name(char* name) {
	uint i = 0;
	uint temp = 0;
	uint len = strlen(name);
	char* itog = NULL;
	while (name[i] != '\0') {
		if (name[i] == '.') {
			temp = i;
		}
		++i;
	}
	itog = malloc(sizeof(char)*(len + 5));
	strcpy(itog, name);
	if (temp == 0) {
		temp = len;
		rash = malloc(sizeof(uchar) * 3);
		memmove(rash, "non", 3);
		size_rash = 3;
		kolvo_meta += 3;
	}
	else {
		size_rash = i - temp - 1;
		kolvo_meta += size_rash;
		rash = malloc(sizeof(uchar)*i - temp - 1);
		for (uint j = temp; j < temp + size_rash; ++j) {
			rash[j - temp] = name[j + 1];
		}
	}
	itog[temp] = '.';
	itog[temp + 1] = 'a';
	itog[temp + 2] = 'r';
	itog[temp + 3] = 'h';
	itog[temp + 4] = '\0';
	return itog;
}

void write_byte(uchar *byte, uchar numb, uchar start, uchar start_byte, uchar dlina) { /*1 запаковывающийся байт, 2 элемент таблицы,
	                                                                                 3 сколько бит из кода символа записанно в файл
																					 4 cколько бит заполненно в текущем блоке, 5 длина кода*/
	for (int i = start; i < start + 8 - start_byte && i < start + dlina; i++) {//пока меньше конца байта и пока мешьше длины кода
		*byte |= table[numb].kod[i] << (7 - start_byte - (i - start));
	}
}//запишет в байт биты от стартовой точки и до заданного конца

int compare(const void * x1, const void * x2) 
{
	return (*(int*)x2 - *(int*)x1);              
}


void create_table(drevo *hafman, uint numb) {
	if (hafman->left == NULL && hafman->right == NULL) {
		table[now_power].dlina = numb;
		table[now_power].simvol = hafman->simvol;
		memmove(table[now_power].kod, code, numb);
		++now_power;
	}
	else {
		code[numb] = 0;
		++numb;
		create_table(hafman->left, numb);
		--numb;
		code[numb] = 1;
		++numb;
		create_table(hafman->right, numb);
	}
}

FILE* meta(char* name) { //записываем метаданные
	FILE* myfile = fopen(name, "wb");
	uchar logo[] = { "archangel506" };//метка что архивированно именно этой программой
	fwrite(logo, 1, 13, myfile);
	fwrite(&kolvo_meta, 4, 1, myfile);
	fwrite(&kolvo, 4, 1, myfile);//количество символов
	fwrite(&power, 4, 1, myfile);//мощность алфавита
	fwrite(table, sizeof(shablon), power, myfile);
	fwrite(&size_rash, 1, 1, myfile);//запомнили расширение изначального файла
	fwrite(rash, 1, size_rash, myfile);
	free(rash);
	return myfile;
}

void archiv(char* name) {
	FILE* myfile = fopen(name, "rb");
	FILE* code = NULL;
	if (myfile == NULL) {
		return;
	}
	uint chastota[N] = { 0 };
	uint chastota_sort[N] = { 0 };
	uchar simvol[N] = { 0 };
	uint i;
	uchar temp;
	uchar unpack;
	char* itog = NULL;
	while (!feof(myfile)) {
		fread(&temp, 1, 1, myfile);
		if (feof(myfile)) {
			break;
		}
		++chastota[temp];
		++kolvo;
	}
	fclose(myfile);
	if (kolvo == 0) {
		return;
	}
	for (i = 0; i < N; ++i) {
		if (chastota[i] > 0) {
			++power;
		}
	}
	memmove(chastota_sort, chastota, N*sizeof(uint));
	qsort(chastota_sort, N, sizeof(uint), compare);
	for (i = 0; i < power; ++i) {
		for (uint j = 0; j < N; ++j) {
			if (chastota_sort[i] == chastota[j]) {
				simvol[i] = j;
				chastota[j] = 0;
				break;
			}
		}
	}//отсортировали в порядке убывания частоты и записали символы под теми же индексами
	drevo *NZ = malloc(sizeof(drevo)*power);
	spisok *postr = NULL;//список на построение дерева
	spisok *support;
	spisok *support_two;
	spisok *support_three;
	for (i = 0; i < power; ++i) {
		NZ[i].chastota = chastota_sort[i];
		NZ[i].simvol = simvol[i];
		NZ[i].left = NULL;
		NZ[i].right = NULL;
		support = postr;
		postr = malloc(sizeof(spisok));
		postr->chastota = chastota_sort[i];
		postr->next = support;
		postr->vetka = &NZ[i];
	}
	uint kontrol = power;
	drevo *hafman = NULL;
	while (kontrol != 1) {
		hafman = malloc(sizeof(drevo));
		hafman->left = postr->next->vetka;
		hafman->right = postr->vetka;
		hafman->chastota = postr->chastota + postr->next->chastota;
		hafman->simvol = 0;// создал узел дерева
		support = malloc(sizeof(spisok));
		support->next = postr->next->next;
		support->chastota = postr->chastota + postr->next->chastota;
		support->vetka = hafman;
		support_two = postr->next;
		free(postr);
		free(support_two);//удалил два последних элемента списка и создал общий
		postr = support;
		support = postr->next;
		support_two = postr->next;
		while (support) {
			if (postr->chastota > support->chastota) {
				if (support->next == NULL) {
					support->next = postr;
					postr = postr->next;
					support->next->next = NULL;
					break;
				}
			}
			else {
				if (support_two == support) {
					break;
				}
				else {
					support_three = postr;
					postr = postr->next;
					support_three->next = support;
					support_two->next = support_three;
					break;
				}
			}
			support_two = support;
			support = support->next;
		}//отсортировали список
		--kontrol;
	}//построили дерефо хаффмана
	hafman = postr->vetka;
	free(postr);
	table = malloc(sizeof(shablon)*power);
	kolvo_meta += sizeof(shablon)*power;
	create_table(hafman, 0);//создали таблицу для архивации
	free(NZ);
	itog = create_name(name);//имя сжатого файла
	kolvo_meta += 9;
	code = meta(itog);
	myfile = fopen(name, "rb");
	uchar byte = 0; //"запаковывающийся" байт
	uchar bit_write = 0;//cколько бит заполненно в текущем блоке
	uchar bit_in_file = 0;//сколько бит из кода символа записанно в файл
	uchar help = 0;
	while (!feof(myfile)) {
		fread(&unpack, 1, 1, myfile);
		if (feof(myfile)) {
			fwrite(&byte, 1, 1, code);
			fclose(myfile);
			fclose(code);
			break;
		}
		for (i = 0; i < power; ++i) {
			if (table[i].simvol == unpack) {
				while (bit_in_file != table[i].dlina) {
					if (8 - bit_write >= table[i].dlina - bit_in_file) {//сколько бит свободно >= которых нужно записать
						write_byte(&byte, i, bit_in_file, bit_write, table[i].dlina - bit_in_file); /*1 запаковывающийся байт, 2 элемент таблицы,
																									3 сколько бит из кода символа записанно в файл
																									4 cколько бит заполненно в текущем блоке, 5 длина кода*/
						bit_write += table[i].dlina - bit_in_file;
						bit_in_file += table[i].dlina - bit_in_file;
						if (bit_write == 8) {
							fwrite(&byte, 1, 1, code);
							byte = 0;
							bit_write = 0;
						}
					}
					else {
						write_byte(&byte, i, bit_in_file, bit_write, table[i].dlina - bit_in_file);
						fwrite(&byte, 1, 1, code);
						bit_in_file += 8 - bit_write;
						bit_write = 0;
						byte = 0;
					}
				}
				bit_in_file = 0;
				break;
			}
		}
	}
	printf("successfull");
}