﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "arhivator.h"

uchar* raz_name(char* name, uchar *rash, uchar size) {
	uint i = 0;
	uint temp;
	uint len = strlen(name);
	uint j;
	uchar *itog = malloc(sizeof(uchar)*(2 * len + size));
	if (strcmp(rash, "non") == 0) {
		strcpy(itog, name);
		itog[len] = '\0';
	}
	else {
		while (name[i] != '\0') {
			if (name[i] == '.') {
				temp = i;
			}
			++i;
		}
		len = i;
		for (i = 0; i <= temp; ++i) {
			itog[i] = name[i];
		}
		for (j = i; j < i + size; ++j) {
			itog[j] = rash[j - i];
		}
		itog[j] = '\0';
	}
	return itog;
}

void clearn(uchar *stroka) {
	for (int i = 0; i < N; ++i) {
		stroka[i] = 2;
	}
}

uchar load_bit(uchar byte, uchar numb) {
	return (byte >> (7 - numb)) & 1;
}

uchar sravnenie(shablon *table, uchar* block, uint power, uchar *simvol) {
	uint flag = 0;
	for (uint i = 0; i < power; ++i) {
		for (uchar j = 0; j < table[i].dlina; ++j) {
			if (table[i].kod[j] != block[j]) {
				flag = 0;
				break;
			}
			++flag;
		}
		if (flag == table[i].dlina) {//если код совпал
			*simvol = table[i].simvol;
			return 1;
		}
	}
	return 0;
}

void razarhiv(char* name) {
	uchar firma[13] = { 0 };
	FILE* myfile;
	FILE* code;
	myfile = fopen(name, "rb");
	uchar size_rash = 0;
	if (myfile == NULL) {
		printf("Incorrect name file\n");
		return;
	}
	uint size_file = 0;
	fseek(myfile, 0, SEEK_END);
	size_file = ftell(myfile);
	fseek(myfile, 0, SEEK_SET);
	if (13 != fread(firma, 1, 13, myfile){
		fclose(myfile);
		return;
	}
	if (strcmp(firma, "archangel506") == 0) {
		uint kolvo_meta = 0;
		if (1 != fread(&kolvo_meta, 4, 1, myfile) {
			fclose(myfile);
			return;
		}		
		if (kolvo_meta >= size_file - 17) {
			fclose(myfile);
			return;
		}
		uchar* mass = malloc(kolvo_meta);
		fread(mass, 1, kolvo_meta, myfile);
		uchar *help = mass;
		uint kolvo = 0;
		uint power = 0;
		uint *ykaz = (uint*)(help);
		kolvo = *ykaz;
		help += 4;
		ykaz = (uint*)(help);
		power = *ykaz;
		help += 4;
		shablon *table = malloc(sizeof(shablon)*power);
		uchar simvol = 0;
		memmove(table, help, sizeof(shablon)*power);
		for (int i = 0; i < power; ++i) {
			printf("%d %c ", table[i].dlina, table[i].simvol);
			for (int j = 0; j < table[i].dlina; ++j) {
				printf("%d", table[i].kod[j]);
			}
			printf("\n");
		}
		help += sizeof(shablon)*power;
		size_rash = *help;
		++help;
		uchar *rash = malloc(sizeof(uchar)*size_rash);
		memmove(rash, help, sizeof(uchar)*size_rash);
		help += size_rash;
		int i = 0;
		free(mass);
		uchar byte = 0;//распаковывающийся байт
		uint schet = 0;
		uchar sled = 0;//номер распаковывающегося бита
		uchar kontrol = 0;
		uchar temp = 0;
		uchar *itog = raz_name(name, rash, size_rash);//создание имени итогового файла
		free(rash);
		uchar block[N];
		clearn(block);
		code = fopen(itog, "wb");
		while (!feof(myfile) && schet < kolvo) {
			fread(&byte, 1, 1, myfile);
			if (feof(myfile)) {
				fclose(myfile);
				fclose(code);
				break;
			}
			while (sled != 8 && schet < kolvo) {
				block[kontrol] = load_bit(byte, sled);
				if (sravnenie(table, block, power, &simvol)) {
					fwrite(&simvol, 1, 1, code);
					clearn(block);
					kontrol = 0;
					++sled;
					++schet;
				}
				else {
					++sled;
					++kontrol;
				}
			}
			sled = 0;
		}
		fclose(myfile);
		fclose(code);
		printf("successfull");
	}
	else {
		printf("Incorrect file");
		return;
	}
}