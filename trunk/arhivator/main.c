#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include "arhivator.h"
#include "razarhivator.h"

int main(int argc, char* argv[]) {
	if (argc != 3) {
		printf("Incorrect command\n");
		return 1;
	}
	FILE* myfile = NULL;
	myfile = fopen(argv[2], "rb");
	if (myfile == NULL) {
		printf("Incorrect name file\n");
		return 1;
	}
	else { 
		fclose(myfile);
	}
	if (strcmp(argv[1], "-a") == 0) {
		archiv(argv[2]);
	}
	else {
		if (strcmp(argv[1], "-r") == 0) {
			razarhiv(argv[2]);
		}
		else {
			printf("Incorrect command\n");
			return 1;
		}
	}
	return 0;
}