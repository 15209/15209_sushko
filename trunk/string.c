#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <windows.h>
#define N 256

unsigned char eng_one[] = "abvgdezijklmnoprstufxABVGDEZIJKLMNOPRSTUFX``" ;
unsigned char rus_one[] = "��������������������������������������������" ;
unsigned char eng_two[] = "yozhczchsh``y`e`yuyaYOZHCZCHSH``Y`E`YUYA" ;
unsigned char rus_two[] = "��������������������" ;
unsigned char *zamena(unsigned char *str, unsigned char *y, unsigned char *z) {
	static unsigned char x[N];
	strcpy(x, str);
	int lenx=strlen(x);
	int leny=strlen(y);
	int lenz=strlen(z);
	int j;
	int mesto;
	int i;
	int sum = 0;
	int razn;
	int sh;
	for (i = 0; i < lenx - leny + 1; ++i) {
		if (x[i] == y[0]) {
			++sum;
			mesto = i;
			for (j = i + 1; j < i + leny; ++j) {
				if (x[j] == y[j-i]) {
					++sum;
				}
				else {
					sum = 0;
					break;
				}
			}
		}
		if (sum == leny) {
			if (leny > lenz) {
				razn = leny - lenz;
				for (sh = mesto + leny; sh <= lenx; ++sh) {
					x[sh - razn] = x[sh];
				}

				for (sh = mesto; sh < mesto + lenz; ++sh) {
					x[sh] = z[sh - mesto];
				}
				x[lenx - razn] = '\0';
				lenx -= razn; 
			}
			if (leny < lenz) {
				razn = lenz - leny;
				for (sh = lenx - 1; sh > mesto; --sh) {
					x[sh + razn] = x[sh];
				}
				for (sh = mesto; sh < mesto + lenz; ++sh) {
					x[sh] = z[sh - mesto];
				}
				x[lenx + razn] = '\0';
				lenx += razn; 
			}
			if (leny == lenz) {
				for (sh = mesto; sh < mesto + lenz; ++sh) {
					x[sh] = z[sh - mesto];
				}
			}
			sum = 0;
		}
	}
	return x;
}

unsigned char *kapitalizachya(unsigned char *str) {
	static unsigned char x[N];
	strcpy(x, str);
	int lenx = strlen(x);
	int i;
	if (x[0] >= 'a' && x[0] <= 'z') {
		x[0] = toupper(x[0]);
	}
	if (x[0] >= 224) {
		x[0] += '�';
	}
	for (i = 0; i < lenx - 2; ++i) {
		if (x[i] == '.' || x[i] == '!' || x[i] == '?') {
			if (x[i + 2] >= 'a' && x[i + 2] <= 'z') {
				x[i + 2] = toupper(x[i + 2]);
			}
			if (x[i + 2] >= 224) {
				x[i + 2] += '�';
			}
		}
	}
	return x;
}

unsigned char *probely(unsigned char *str) {
	static unsigned char x[N];
	strcpy(x, str);
	int lenx = strlen(x);
	int i=1;
	int j;
	while (i != lenx) {
		if ((x[i] == ' ') && (x[i - 1]== ' ')) {
			for (j = i; j < lenx; ++j) {
				x[j - 1] = x[j];
			}
			i = 1;
			--lenx;
			
		}
		++i;
	}
	i = 0;
	while (i != lenx-1) {
		if ((x[i] == '.' || x[i] == ',' || x[i] == ';' || x[i] == ':' || x[i] == '!' || x[i] == '?') && (x[i + 1] != ' ')) {
			for (j = lenx - 1; j > i; --j) {
				x[j + 1] = x[j];
			}
			x[i + 1] = ' ';
			i = 0;
		}
		++i;
	}
	i = 0;
	while (i != lenx) {
		if ((x[i] == '.' || x[i] == ';' || x[i] == ',' || x[i] == ':' || x[i] == '!' || x[i] == '?') && (x[i - 1] == ' ')) {
			for (j = i; j < lenx; ++j) {
				x[j - 1] = x[j];
			}
			if (x[lenx - 1] == x[lenx - 2]) {
				x[lenx - 1] = ' ';
			}
			i = 0;
		}
		++i;
	}
	x[lenx] = '\0';
	return x;
}

unsigned char *translit(unsigned char *str) {
	static unsigned char x[N];
	strcpy(x, str);
	int lenx = strlen(x);
	int i;
	int j;
	int sch;
	for (i = 0; i < lenx; ++i) {
		if (x[i] == 249) {
			for (j = lenx - 1; j > i; --j) {
				x[j + 2] = x[j];
			}
			x[lenx + 2] = '\0';
			lenx += 2;
			x[i] = 's';
			x[i + 1] = 'h';
			x[i + 2] = 'h';
		}
		if (x[i] == 217) {
			for (j = lenx - 1; j > i; --j) {
				x[j + 2] = x[j];
			}
			x[lenx + 2] = '\0';
			lenx += 2;
			x[i] = 'S';
			x[i + 1] = 'H';
			x[i + 2] = 'H';
		}
	}
	for (i = 0; i < lenx; ++i) {
		for (j = 0; j < 21; ++j) {
			if (x[i] == rus_two[j]) {
				for (sch = lenx - 1; sch > i; --sch) {
					x[sch + 1] = x[sch];
				}
				x[i] = eng_two[j + j];
				x[i + 1] = eng_two[j+j+1];
				x[lenx + 1] = '\0';
				++lenx;

			}
		}
	}
	lenx = strlen(x);
	for (i = 0; i < lenx; ++i) {
		for (j = 0; j < 45; ++j) {
			if (x[i] == rus_one[j]) {
				x[i] = eng_one[j];
				break;
			}
		}
	}
	return x;
}

unsigned char *obr_translit(unsigned char *str) {
	static unsigned char x[N];
	strcpy(x, str);
	int i;
	int j;
	int lenx = strlen(x);
	int sh;
	for (i = 0; i < lenx - 2; ++i) {
		if ((x[i] == 's') && (x[i + 1] == 'h') && (x[i + 2] == 'h')) {
			for (j = i + 3; j < lenx; ++j) {
				x[j - 2] = x[j];
			}
			x[lenx - 2] = '\0';
			x[i] = 249;
			lenx -= 2;
		}
		if ((x[i] == 'S') && (x[i + 1] == 'H') && (x[i + 2] == 'H')) {
			for (j = i + 3; j < lenx; ++j) {
				x[j - 2] = x[j];
			}
			x[lenx - 2] = '\0';
			x[i] = 217;
			lenx -= 2;
		}
	}
	for (i = 0; i < lenx; ++i) {
		for (j = 0; j < 41; j += 2) {
			if ((x[i] == eng_two[j]) && (x[i + 1] == eng_two[j + 1])) {
				for (sh = i + 1; sh < lenx; ++sh) {
					x[sh] = x[sh + 1];
				}
				x[lenx - 1] = '\0';
				--lenx;
				x[i] = rus_two[j/2];
				break;
			}
		}
	}
	for (i = 0; i < lenx; ++i) {
		for (j = 0; j < 45; ++j) {
			if (x[i] == eng_one[j]) {
				x[i] = rus_one[j];
			}
		}
	}
	return x;
}

	int main() {
		unsigned char stroka[N], podstr1[N], podstr2[N];
		SetConsoleCP(1251);// ��������� ������� �������� win-cp 1251 � ����� �����
		SetConsoleOutputCP(1251); // ��������� ������� �������� win-cp 1251 � ����� ������
		printf ("Vvedite stroky\n");
		gets (stroka);
		printf ("\nVvedite slovo kotoroe hotite zamenit\n");
		gets (podstr1);
		printf ("\nVvedite slovo na kotoroe hotite zamenit\n");
		gets (podstr2);
		puts(zamena(stroka, podstr1, podstr2));
		puts(probely(stroka));
		puts (kapitalizachya(probely(stroka)));
		puts(translit(stroka));
		puts(obr_translit(stroka));
		return 0;
	}
