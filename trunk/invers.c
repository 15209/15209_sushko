#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
int main (int argc, char *argv[]) {
	int i; 
	int N;
	int e = 0;
	int elem;
	int change;
	int tru = 1;
	int fact = 1;
	int *perest= NULL;
	int *inver = NULL;
	if (argc == 2) {
			N = atoi(argv[1]);
	}
	else {
		printf("Write length inversion: ");
		scanf("%i", &N);
	}
	if (N < 1) {
		printf("Incorrect number");
		return 0;
	}
	for (i = 2; i <= N; ++i) {
		fact = fact*i;
	}
	perest = (int*) calloc (N, sizeof(int));
	inver = (int*) calloc (N, sizeof(int));
	/*for (i = 0; i < N; ++i) {


	}*/
	if (perest == NULL || inver == NULL) {
		printf("Memory is not allocated");
		return 0;
	}
	while (fact > 0) {
		while (e < N) {
			perest[e] = N - e;
			for (i = e; i > inver[N - e - 1]; --i) {
				change = perest[i];
				perest[i] = perest[i - 1];
				perest[i - 1] = change;
			}
			++e;
		}
		e = 0;
		for (i = 0; i < N; ++i) {
			if (perest[i] <= 9) {
				printf("%i ", perest[i]);
				perest[i] = 0;
			}
			else
			{
				perest[i] = perest[i] + 'A' - 10;
				printf("%c ", perest[i]);
			}
		}
		printf("\n");
		i = N - 2;
		while (tru == 1) {
			if (i > 0) {
				break;
			}
			elem = ++inver[i];
			if (elem >= N - i) {
				inver[i] = 0;
				--i;
			}
			else {
				inver[i] = elem;
				tru = 0;
			}
			printf("%i", inver[i]);
		}
		printf("\n");
		elem = 0;
		tru = 1;
		--fact;
	}
	free (perest);
	free (inver);
	return 0;
} 