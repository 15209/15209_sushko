﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#ifdef _WIN32 
#include <windows.h>
#endif
#define N 256

typedef unsigned char uchar;
int netprobela(uchar *str, int i) {
	if ((str[i] == '.' || str[i] == ',' || str[i] == ';' || str[i] == ':' || str[i] == '!' || str[i] == '?') && (str[i + 1] != ' ') && (str[i + 1] != '\0')) {
		return 1;
	}
	else {
		return 0;
	}
}
int prob_peredznak(uchar *str, int j) {
	if ((str[j] == '.' || str[j] == ';' || str[j] == ',' || str[j] == ':' || str[j] == '!' || str[j] == '?') && (str[j - 1] == ' ')) {
		return 1;
	}
	else {
		return 0;
	}
}
int podr_prob(uchar *str, int j) {
	if ((str[j] == ' ') && (str[j + 1] == ' ')) {
		return 1;
	}
	else {
		return 0;
	}
}
uchar *zamena(uchar *x, uchar *y, uchar *z) {
	int i = 0;
	int j;
	int k = 0;
	uchar *str = NULL;
	int lenx = 0;
	int leny = strlen(y); 
	int lenz = strlen(z);
	int razn = leny - lenz;
	int sum = 0;
	int sch;
 	while (x[i] != '\0') {
		if (x[i] == y[0]) {
			++sum;
			for (j = i + 1; y[j - i] != '\0'; ++j) {
				if (x[j] == y[j - i]) {
					++sum;
				}
				else {
					sum = 0;
					break;
				}
			}
			if (sum == leny) {
				lenx -= razn;
				sum = 0;
			}
		}
		++lenx;
		++i;
	}
	str = (uchar*) malloc((lenx + 1)*sizeof(uchar));
	str[lenx] = '\0';
	i = 0;
	j = 0;
	sum = 0;
	while (str[i] != '\0') {
		if (x[j] != y[0]) {
			str[i] = x[j];
		}
		else {
			sch = j;
			++sum;
			str[i] = x[j];
			if (razn >= 0) {
					for (k = sch + 1; y[k-sch] != '\0'; ++k) {
						if (x[k] == y[k - sch]) {
							++sum;
						}
						else {
							sum = 0;
							str[i] = x[j];
							break;
						}
					}
					if (sum == leny) {
						k = sch;
						while (z[k - sch] != '\0') {
							str[i] = z[k - sch];
							++i;
							++k;
						}
						--i;
						j += leny - 1;
						sum = 0;
					}
			}
				else {
					for (k = sch + 1; k < sch + leny; ++k) {
						if (x[k] == y[k - sch]) {
							++sum;
						}
					}
					if (sum == leny) {
						k = sch;
						while (z[k - sch] != '\0') {
							str[i] = z[k - sch];
							++i;
							++k;
						}
						--i;
						j += leny - 1;
						sum = 0;
					}
					else {
						sum = 0;
					}
				}
			}
		++i;
		++j;
	}
     return str;
}

uchar *kapitalizachya(uchar *x) {
	int lenx = strlen(x);
	uchar *str;
	str = (uchar*)malloc((lenx+1)*sizeof(uchar));
	str[lenx] = '\0';
	int i = 2;
	if (x[0] >= 'a' && x[0] <= 'z') {
			str[0] = toupper(x[0]);
		}
	else {
			str[0] = x[0];
		}
	if (x[0] >= 224) {
			str[0] = x[0] + 'а';
		}
	str[1] = x[1];
	while (str[i] != '\0') {
		if (x[i - 2] == '.' || x[i - 2] == '!' || x[i - 2] == '?') {
			if (x[i] >= 'a' && x[i] <= 'z') {
					str[i] = toupper(x[i]);
				}
			if (x[i] >= 224) {
					str[i] = x[i] + 'а';
				}
		}
		else {
			str[i] = x[i];
		}
		++i;
	}
	return str;
}

uchar *probely(uchar *str) {
	int i = 0;
	int lenx = 0;
	int iznlen = strlen(str);
	uchar *x = NULL;
	int sdvig = 0;
	int sdvig1 = 0;
	int sdvig_two = 0;
	int j;
	int k = 0;
	while (str[i] != '\0') {
		if (netprobela(str, i))  {
			++lenx;
		}
		if (prob_peredznak(str, i)) {
			--lenx;
			j = i - 1;
			while (j>-1) {
				if ((str[j] == ' ') && (str[j - 1] == ' ')) {
					--lenx;
					--j;
				}
				else {
					break;
				}
			}
		}
		if (podr_prob(str, i)) {
			while (str[i + 1] == ' ') {
				--lenx;
				++i;
			}
		}
		++lenx;
		++i;
	}
	if ((str[0] == ' ') && (str[1] == ' ')) {
		--lenx;
	}
	x = (uchar*)malloc((lenx + 1)*sizeof(uchar));
	x[lenx] = '\0';
	i = 0;
	j = 0;
	if (podr_prob(str, i)) {
		++j;
		for (i = 1; i < lenx; ++i) {
			if (podr_prob(str, i)) {
				++j;
			}
			else {
				break;
			}
		}
		i = 0;
	}
	while (x[i] != '\0') {
		x[i] = str[j];
		if (podr_prob(str, j)) {
			++j;
			while (str[j + 1] == ' ') {
				++j;
			}
		}
 		if (prob_peredznak(str, j)) {
			--i;
			k = j - 2;
			while (str[k] == ' ') {
				--i;
				--k;
			}
			x[i] = str[j];
		}
		if (netprobela(str, j)) {
			x[i] = str[j];
			x[i + 1] = ' ';
			--j;
			++i;
		}
		++j;
		++i;
	}
	if (prob_peredznak(str, iznlen-1)) {
		x[i - 1] = str[iznlen - 1];
	}
	return x;
}

uchar *translit(uchar *str) {
	uchar *x=NULL;
	int i;
	int j = 0;
	int iznlen = strlen(str);
	int lenx = 0;
	int tran[256];
	int two_tran[256];
	int three_tran[256];
	int two = 256;
	int three = 257;
	for (i = 0; i < 256; ++i) {
		tran[i] = i;
	}
	tran[192] = 'A';
	tran[193] = 'B';
	tran[194] = 'V';
	tran[195] = 'G';
	tran[196] = 'D';
	tran[197] = 'E';
	tran[168] = two;
	tran[198] = two;
	tran[199] = 'Z';
	tran[200] = 'I';
	tran[201] = 'J';
	tran[202] = 'K';
	tran[203] = 'L';
	tran[204] = 'M';
	tran[205] = 'N';
	tran[206] = 'O';
	tran[207] = 'P';
	tran[208] = 'R';
	tran[209] = 'S';
	tran[210] = 'T';
	tran[211] = 'U';
	tran[212] = 'F';
	tran[213] = 'X';
	tran[214] = two;
	tran[215] = two;
	tran[216] = two;
	tran[217] = three;
	tran[218] = two;
	tran[219] = two;
	tran[220] = '`';
	tran[221] = two;
	tran[222] = two;
	tran[223] = two;
	tran[224] = 'a';
	tran[225] = 'b';
	tran[226] = 'v';
	tran[227] = 'g';
	tran[228] = 'd';
	tran[229] = 'e';
	tran[184] = two;
	tran[230] = two;
	tran[231] = 'z';
	tran[232] = 'i';
	tran[233] = 'j';
	tran[234] = 'k';
	tran[235] = 'l';
	tran[236] = 'm';
	tran[237] = 'n';
	tran[238] = 'o';
	tran[239] = 'p';
	tran[240] = 'r';
	tran[241] = 's';
	tran[242] = 't';
	tran[243] = 'u';
	tran[244] = 'f';
	tran[245] = 'x';
	tran[246] = two;
	tran[247] = two;
	tran[248] = two;
	tran[249] = three;
	tran[250] = two;
	tran[251] = two;
	tran[252] = '`';
	tran[253] = two;
	tran[254] = two;
	tran[255] = two;
	two_tran[168] = 'YO';
	two_tran[198] = 'ZH';
	two_tran[214] = 'CZ';
	two_tran[215] = 'CH';
	two_tran[216] = 'SH';
	two_tran[218] = '``';
	two_tran[219] = 'Y`';
	two_tran[221] = 'E`';
	two_tran[222] = 'YU';
	two_tran[223] = 'YA';
	two_tran[184] = 'yo';
	two_tran[230] = 'zh';
	two_tran[246] = 'cz';
	two_tran[247] = 'ch';
	two_tran[248] = 'sh';
	two_tran[250] = '``';
	two_tran[251] = 'y`';
	two_tran[253] = 'e`';
	two_tran[254] = 'yu';
	two_tran[255] = 'ya';
	three_tran[217] = 'SHH';
	three_tran[249] = 'shh';
	i = 0;
	while (str[i] != '\0') {
		if (tran[str[i]] == two) {
			++lenx;
		}
		if (tran[str[i]] == three) {
			lenx += 2;
		}
		++lenx;
		++i;
	}
	x = (uchar*)malloc((lenx+1)*sizeof(uchar));
	x[lenx] = '\0';
	i = 0;
	while (x[i] != '\0') {
		x[i] = tran[str[j]];
		if (tran[str[j]] == three) {
			x[i] = three_tran[str[j]] >> 16;
			x[i + 1] = (three_tran[str[j]] >> 8) & 8;
			x[i + 2] = three_tran[str[j]] & 0xff;
			i += 2;
		}
		if (tran[str[j]] == two) {
			x[i] = two_tran[str[j]] >> 8;
			x[i + 1] = two_tran[str[j]] & 0xff;
			++i;
		}
		++i;
		++j;
	}
	return x;
}

uchar *obr_translit(uchar *str) {
	uchar eng_one[] = "abvgdezijklmnoprstufxABVGDEZIJKLMNOPRSTUFX``";
	uchar rus_one[] = "абвгдезийклмнопрстуфхАБВГДЕЗИЙКЛМНОПРСТУФХьЬ";
	uchar eng_two[] = "yozhczchsh``y`e`yuyaYOZHCZCHSH``Y`E`YUYA";
	uchar rus_two[] = "ёжцчшъыэюяЁЖЦЧШЪЫЭЮЯ";
	uchar *x = NULL;
	int i = 0;
	int j = 0;
	int k;
	int lenx = 0;
	while (str[i] != '\0') {
		if (str[i] == 's' && str[i + 1] == 'h' && str[i + 2] == 'h') {
			lenx -= 2;
		}
		if (str[i] == 'S' && str[i + 1] == 'H' && str[i + 2] == 'H') {
			lenx -= 2;
		}
		for (j = 0; eng_two[j] != '\0'; j += 2) {
			if ((str[i] == eng_two[j]) && (str[i + 1] == eng_two[j + 1])) {
				--lenx;
				break;
			}
		}
		++lenx;
		++i;
	}
	x = (uchar*)malloc((lenx + 1)*sizeof(uchar));
	i = 0; 
	j = 0;
	while (x[i] != '\0') {
		x[i] = str[j];
		for (k = 0; k < 45; ++k) {
			if (str[j] == eng_one[k]) {
				x[i] = rus_one[k];
				break;
			}
		}
		for (k = 0; k < 41; k += 2) {
			if ((str[j] == eng_two[k]) && (str[j + 1] == eng_two[k + 1])) {
				x[i] = rus_two[k / 2];
				++j;
				break;
			}
		}
		if (str[j] == 's' && str[j + 1] == 'h' && str[j + 2] == 'h') {
			x[i] = 184;
			j += 2;
		}
		if (str[j] == 'S' && str[j + 1] == 'H' && str[j + 2] == 'H') {
			x[i] = 168;
			j += 2;
		}
		++j;
		++i;
	}
	return x;
}



int main() {
	uchar stroka[N], podstr1[N], podstr2[N];
	uchar *v_zam = NULL;
	uchar *v_kap = NULL;
	uchar *v_prob = NULL;
	uchar *v_trans = NULL;
	uchar *v_obrtrans = NULL;
#ifdef _WIN32
	SetConsoleCP(1251);// установка кодовой страницы win-cp 1251 в поток ввода
	SetConsoleOutputCP(1251); // установка кодовой страницы win-cp 1251 в поток вывода
#endif
	printf("Vvedite stroky\n");
	gets(stroka);
    printf("\nVvedite slovo kotoroe hotite zamenit\n");
	gets(podstr1);
	printf("\nVvedite slovo na kotoroe hotite zamenit\n");
	gets(podstr2);
	v_zam = zamena(stroka, podstr1, podstr2);
    v_prob = probely(v_zam);
	v_kap = kapitalizachya(v_prob);
	v_trans = translit(v_kap);
	v_obrtrans = obr_translit(v_trans);
	puts(v_zam);
	puts(v_kap);
	puts(v_prob);
	puts(v_trans);
	puts(v_obrtrans);
	free(v_zam);
	free(v_kap);
	free(v_prob);
	free(v_trans);
	free(v_obrtrans);
	return 0;
}