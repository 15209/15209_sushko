#define _CRT_SECURE_NO_WARNINGS //need only in visual studio
#define N 100
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main() {
	int i, n, from, to, j, z = 0, r;
	char x[N] = { 0 };
	int y[N];
	printf("Follow the instructions:\n1) Write 'from'(1<from<37)\n2) Write 'to' (1<to<37)\n3) Write 'x'\n");
	scanf("%i%i%s", &from, &to, &x);
	if (from == to) {
		printf("'from'='to', so 'x'='y'=%s", x);
		return 0;
	}
	if (from <= 1 || to <= 1 || from > 36 || to > 36) {
		printf("Incorrect from or to. Restart programm and try again.");
		return 0;
	}
	n = strlen(x);
	char reg[] = "abcdefghijklmnopqrstuvwxyz";
	int regkol = 26;
	for (i = 0; i < n; ++i) {
		for (j = 0; j < regkol; ++j) {
			if (x[i] == reg[j]) {
				x[i] = toupper(x[i]);
			}
		}
	}
	char num[] = "0123456789ABCDEFGHIJKLMOPQRSTUVWXYZ";
	int numkol = 36;
	int st = 1;
	for (i = 0; i < n; ++i) {
		for (j = 0; j < numkol; ++j) {
			if (x[i] == num[j]) {
				if (x[i] > 64 && x[i] < 91) {
					z = z + (x[i] - 55)*st;
					if (x[i] - 55 >= from) {
						printf("Incorrect 'from' or 'x'. Try again.");
						return 0;
					}
				}
				else {
					z = z + (x[i] - 48)*st;
					if (x[i] - 48 >= from) {
						printf("Incorrect 'from' or 'x'. Try again.");
						return 0;
					}
				}
			}
		}
		st = from*st;
	}
	if (to == 10) {
		printf("%i", z);
		return 0;
	}
	else {
		r = -1;
		while (z != 0) {
			++r;
			y[r] = z % to;
			z = z / 10;
		}
		for (i = 0; i < r; ++i) {
			if (y[i] < 10) {
				y[i] = y[i] + 48;
			}
			else
			{
				y[i] = y[i] + 55;
			}
		}
	}
	for (i = N - 1; i != 0; --i) {
		if (y[i] > 0) {
			printf("%c", y[i]);
		}
	}
	return 0;
}