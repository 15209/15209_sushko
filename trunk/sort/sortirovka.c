#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <locale.h>
#define N 1000
int stolb;
char **global = NULL;
int kol_file = 0;
int proverka_stolb(char* mass) {
	int stolb_this_str = 1;
	int j = 0;
	while (stolb_this_str != stolb) {
		if (mass[j] == '\t') {
			++stolb_this_str;
		}
		if (mass[j] == '\0') {
			printf("������� ������� �� ����������");
			return -1;
		}
		++j;
	}
	return j;
}

void spisok(char* x) {
	int len = strlen(x);
	global = (char**)realloc(global, sizeof(char*)*(kol_file+1));
	global[kol_file] = (char*)malloc(sizeof(char)*(len + 1));
	strcpy(global[kol_file], x);
	free(x);
}

int sravn(FILE* file, char* mass, int sch) {
	if (sch == 0) {
		int len = strlen(mass);
		mass[len - 2] = '\0';
		fputs(mass, file);
	}
	else {
		int len = strlen(mass);
		if (mass[len - 1] == '\n') {
			mass[len - 2] = '\0';
		}
			fputs("\r\n", file);
			fputs(mass, file);
	}
	return ++sch;
}
void quick_sort(char *name) {
	//printf("\n");
	int i = 0;
	int end;
	int opor;
	int loc = 0;
	char mass[10][N];
	FILE *file = NULL;
	FILE *left = NULL;
	FILE *right = NULL;
	int sch_left = 0;
	int sch_right = 0;
	char *x = tmpnam(NULL);
	char* y = NULL;
	char* z = NULL;
	int len = strlen(x);
	file = fopen(name, "rb");
	z = (char*)malloc(sizeof(char)*(len + 1));
	strcpy(z, x);
	y = tmpnam(NULL);
	len = strlen(y);
	x = (char*)malloc(sizeof(char)*(len + 1));
	strcpy(x, y);
	int opordop;
	left = fopen(z, "wb");
	right = fopen(x, "wb");
	if (fgets(mass[0], N, file) == NULL) {
		printf("���� ������");
		return;
	}
	while (!feof(file)) {
		i = 1;
		while (i != 10 && !feof(file)) {
			fgets(mass[i], N, file);
			//printf("%s", mass[i]);
			++i;
		}
		end = i;
		i = 1;
		opor = proverka_stolb(mass[0]);
		if (opor == -1) {
			return;
		}
		while (i < end) {
			loc = proverka_stolb(mass[i]);
			if (loc == -1) {
				return;
			}
			opordop = opor;
			while (mass[0][opor] != '\t' && mass[i][loc] != '\t') {
				if (mass[0][opor] != mass[i][loc]) {
					if (mass[0][opor] > mass[i][loc]) {
						sch_left=sravn(left, mass[i], sch_left);
						break;
					}
					if (mass[0][opor] < mass[i][loc]) {
						sch_right=sravn(right, mass[i], sch_right);
						break;
					}
				}
				++opordop;
				++loc;
			}
			if (mass[0][opor - 1] == mass[i][loc - 1] && mass[0][opor - 1] != '\t' && mass[i][loc - 1] != '\t' && mass[0][opor] == '\t' && mass[i][loc] == '\t') {
				sch_right=sravn(right, mass[i], sch_right);
			}
			++i;
		}
	}
	++sch_left;
	len = strlen(mass[0]);
	mass[0][len - 2] = '\0';
	if (sch_left > 1) {
		fputs("\r\n", left);
		fputs(mass[0], left);
	}
	else {
		fputs(mass[0], left);
	}
	fclose(file);
	remove(name);
	fclose(left);
	fclose(right);
	if (sch_left > 1) {
		quick_sort(z);
		free(z);
	}
	else {
		spisok(z);
		++kol_file;
	}
	if (sch_right > 1) {
		quick_sort(x);
		free(x);
	}

	if (sch_right == 1) {
		spisok(x);
		++kol_file;
	}
	if (sch_right == 0) {
		remove(x);
		free(x);
	}
}

void kopy(FILE* temp, FILE* kyda) {
	char buffer[N];
	int len;
	while (!feof(temp)) {
		fgets(buffer, N, temp);
		len = strlen(buffer);
		if (len == N && buffer[len - 1] != '\n') {
			printf("������� ������� ������");
			return;
		}
		fputs(buffer, kyda);
	}
}

FILE* sborka(char *x) {
	int kol=kol_file-1;
	FILE* file = NULL;
	FILE* temp = NULL;
	char buffer_one[N];
	int i = 0;
	file = fopen(x, "wb");
	while (i < kol_file) {
		temp = fopen(global[i], "rb");
		while (!feof(temp)) {
			fgets(buffer_one, N, temp);
			//printf("%s", buffer_one);
			if (i != 0) {
				fputs("\r\n", file);
			}
			fputs(buffer_one, file);
		}
		fclose(temp);
		remove(global[i]);
		free(global[i]);
		++i;
	}
	free(global);
	return file;
}

int main(int argc, char *argv[]) {
	if (argc != 4) {
		printf("error command arguments");
		return 0;
	}
	int len;
	setlocale(LC_ALL, "rus");
	stolb = atoi(argv[3]);
	FILE *myfile = NULL;
	FILE *rezerv = NULL;
	FILE *final_file = NULL;
	char *x = tmpnam(NULL);
	len = strlen(x);
	char* temp = (char*)malloc(sizeof(char)*(len + 1));
	strcpy(temp, x);
	myfile = fopen(argv[1], "rb");
	if (myfile == NULL) {
		printf("������ ������ �� �����");
		return 0;
	}
	rezerv = fopen(temp, "wb");
	kopy(myfile, rezerv);
	fclose(myfile);
	fclose(rezerv);
	quick_sort(temp);
	free(temp);
	final_file = sborka(argv[2]);
	fclose(final_file);
	return 0;
}