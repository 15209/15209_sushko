﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#ifdef _WIN32 
#include <windows.h>
#endif
#define N 1000

typedef struct infor {
	struct infor *next;
	char name[30];
	char fam[30];
	char otch[30];
	char book[30];
} infor; 

unsigned int key(char* fam) {
	unsigned int key;
		for (key = 0; *fam != '\0'; fam++)
			key = *fam + 31 * key;
		return key % 101;
}

free_memory(infor* mass) {
	infor* temp=NULL;
	while (mass->next != NULL) {
		temp = mass;
		mass = mass->next;
		free(temp);
	}
	free(mass);
}

int main(int argc, char* argv[]) {
#ifdef _WIN32
	SetConsoleCP(1251);// установка кодовой страницы win-cp 1251 в поток ввода
	SetConsoleOutputCP(1251); // установка кодовой страницы win-cp 1251 в поток вывода
#endif
	if (argc != 2) {
		printf("Ошибка ввода аргументов командной строки");
		return 0;
	}
	FILE *myfile = NULL;
	int i = 0;
	int j = 0;
	char buffer[N];
	int numb;
	int true;
	myfile = fopen(argv[1], "r");
	if (myfile == NULL) {
		printf("ошибка открытия файла");
		return 0;
	}
	int sch;
	char vvod[30];
	infor mass[100];
	infor* pod_stryct = NULL;
	infor* clone = NULL;
	for (i = 0; i < 101; ++i) {
		mass[i].fam[0] = '\0';
		mass[i].next = NULL;
	}
	while (!feof(myfile)) {
		fgets(buffer, N, myfile);
		i = 0;
		j = 0;
		char name_cl[30];
		char fam_cl[30];
		char otch_cl[30];
		char book_cl[30];
		while (buffer[i] != '\t') {
			fam_cl[j] = buffer[i];
			++i;
			++j;
		}
		fam_cl[j] = '\0';
		numb = key(fam_cl); 
		++i;
		j = 0;
		while (buffer[i] != '\t') {
			name_cl[j] = buffer[i];
			++i;
			++j;
		}
		name_cl[j] = '\0';
		++i;
		j = 0;
		while (buffer[i] != '\t') {
			otch_cl[j] = buffer[i];
			++i;
			++j;
		}
		otch_cl[j] = '\0';
		++i;
		j = 0;
		while (buffer[i] != '\n' && buffer[i] != '\0') {
			book_cl[j] = buffer[i];
			++i;
			++j;
		}
		book_cl[j] = '\0';
					if (mass[numb].fam[0] == '\0' || (strcmp(mass[numb].fam, fam_cl) == 0)) {
						strcpy(mass[numb].fam, fam_cl);
						strcpy(mass[numb].name, name_cl);
						strcpy(mass[numb].otch, otch_cl);
						strcpy(mass[numb].book, book_cl);
					}
					else {
						clone = (infor *)malloc(sizeof(infor));
						strcpy(clone->fam, fam_cl);
						strcpy(clone->name, name_cl);
						strcpy(clone->otch, otch_cl);
						strcpy(clone->book, book_cl);
						clone->next = mass[numb].next;
						mass[numb].next = clone;
					}
	}
	fclose(myfile);
	while (1) {
		printf("Введите фамилию\n");
		gets(vvod);
		if (vvod[0] == '\0') { 
			gets(vvod);
			if (vvod[0] == '\0') {
				break;
			}
		}
		numb = key(vvod);
		printf("%i\n", numb);
		true = 0;
		if (mass[numb].fam[0] == '\0') {
			printf("Такой фамилии в списке нет\n");
		}
		else {
			pod_stryct = &mass[numb];
			while (pod_stryct != NULL) {
				if (strcmp(pod_stryct->fam, vvod) == 0) {
					printf("%s ", pod_stryct->fam);
					printf("%s ", pod_stryct->name);
					printf("%s ", pod_stryct->otch);
					printf("%s\n", pod_stryct->book);
					true = 1;
					break;
				}
				pod_stryct = pod_stryct->next;
			}
			if (true == 0) {
				printf("Такой фамилии в списке нет\n");
			}
		}
	}
	for (i = 0; i < 101; ++i) {
		if (mass[i].next != NULL) {
			free_memory(mass[i].next);
		}
	}
} 