﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

int stroka = 0;
int stolb = 0;
int live_or_die(char **mass, int i, int j) {
	int life = 0;
	int sdvigstr_down;
	int sdvigstr_top;
	int sdvigstolb_down;
	int sdvigstolb_top;
			if (i - 1 < 0) {
			sdvigstr_down = stroka - 1;
			}
			else {
			sdvigstr_down = i - 1;
			}
			if (i + 1 == stroka) {
			sdvigstr_top = 0;
			}
			else {
			sdvigstr_top = i + 1;
			}
			if (j - 1 < 0) {
			sdvigstolb_down = stolb - 1;
			}
			else {
			sdvigstolb_down = j - 1;
			}
			if (j + 1 == stolb) {
			sdvigstolb_top = 0;
			}
			else {
			sdvigstolb_top = j + 1;
			} // посчитали расположение всех клеток вокруг которые могут выйти за границу поля

			if (mass[sdvigstr_down][sdvigstolb_down] == '*' || mass[sdvigstr_down][sdvigstolb_down] == '-') {
				++life;
			}
			if (mass[sdvigstr_down][j] == '*' || mass[sdvigstr_down][j] == '-') {
				++life;
			}
			if (mass[sdvigstr_down][sdvigstolb_top] == '*' || mass[sdvigstr_down][sdvigstolb_top] == '-') {
				++life;
			}
			if (mass[i][sdvigstolb_down] == '*' || mass[i][sdvigstolb_down] == '-') {
				++life;
			}
			if (mass[i][sdvigstolb_top] == '*' || mass[i][sdvigstolb_top] == '-') {
				++life;
			}
			if (mass[sdvigstr_top][sdvigstolb_down] == '*' || mass[sdvigstr_top][sdvigstolb_down] == '-') {
				++life;
			}
			if (mass[sdvigstr_top][j] == '*' || mass[sdvigstr_top][j] == '-') {
				++life;
			}
			if (mass[sdvigstr_top][sdvigstolb_top] == '*' || mass[sdvigstr_top][sdvigstolb_top] == '-') {
				++life;
			}
			return life; // количество живых соседей
}

int main(int argc, char* argv[]) {
	setlocale(LC_ALL, "Russian");
	int i;
	int j; 
	int jump = 1;
	int schlife = 0;
	char gameover;
	int fsize;
	int proverka = 0;
	FILE* myfile=NULL;
	char * mass = NULL;
	char** dvymass = NULL;
	if (argc != 2) {
		printf("Не корректно задан аргумент командной строки");
		return 0;
	}
	myfile = fopen(argv[1], "rb");
	if (myfile == NULL) {
		printf("ошибка чтения из файла");
		return 0;
	}
	fseek(myfile, 0, SEEK_END);
	fsize = ftell(myfile);
	if (fsize == 0) {
		printf("Файл пустой");
		return 0;
	}
	mass = (char*)malloc(fsize*sizeof(char)+1);
	fseek(myfile, 0, SEEK_SET);
	size_t kol = fread(mass, 1, fsize, myfile);
	mass[kol] = '\0';
	fclose(myfile);
	if (kol != fsize) {
		printf("Файл считался некорректно");
		return 0;
	}
	i = 0;
	while (mass[i] != '\0') {
		if (mass[i] == '\r') {
			++stroka;
			--stolb;
			++i;
		}
		++i;
		++stolb;
	}
	++stroka;
	stolb /= stroka;
	dvymass = (char**)malloc(stroka*sizeof(char*));
	dvymass[0] = mass;
	for (i = 1; i < stroka; ++i) {
		dvymass[i] = &mass[(stolb + 2) * jump];
		++jump;
	}
	j = 0;
	while (dvymass[0][j] != '\r') {
		if (dvymass[0][j] == '*' || dvymass[0][j] == ' ') {
				++proverka;
		}
		++j;
	}
	j = 0;
	for (i = 1; i < stroka; ++i) {
		while (dvymass[i][j] != '\r' && dvymass[i][j] != '\0') {
			++j;
		}
		if (j != proverka) {
			printf("Некорректно задан формат поля");
			return 0;
		}
		j = 0;
	}
	while (1) {
		for (i = 0; i < stroka; ++i) {
			for (j = 0; j < stolb; ++j) {
				if (dvymass[i][j] == ' ') {
					schlife = live_or_die(dvymass, i, j);
					if (schlife == 3) {
						dvymass[i][j] = '+';
					}
					schlife = 0;
				}
				if (dvymass[i][j] == '*' || dvymass[i][j] == '-') {
					schlife = live_or_die(dvymass, i, j);
					if (schlife < 2 || schlife > 3) {
						dvymass[i][j] = '-';
					}
					schlife = 0;
				}
			}
		}
		for (i = 0; i < stroka; ++i) {
			for (j = 0; j < stolb; ++j) {
				if (dvymass[i][j] == '+') {
						dvymass[i][j] = '*';
				}
				if (dvymass[i][j] == '-') {
					dvymass[i][j] = ' ';
				}
				printf("%c ", dvymass[i][j]);
			}
			printf("\n");
		}
		printf("\nЕсли хотите завершить игру, нажмите 'f' \n");
		scanf("%c", &gameover);
		if (gameover == 'f') {
			break;
		}
	}
	free(dvymass);
	free(mass);
	return 0;
}