﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>


int n = 0;
int m = 0;
char proverka(char **lab, int start_i, int start_j) {
	if (start_i > -1 && start_j > -1 && start_i < n && start_j < m) {
		if (lab[start_i][start_j] != '#' && lab[start_i][start_j] != 'x' && lab[start_i][start_j] != '*' ) {
			return 1;
		}
		else {
			return 0;
		}
	}
	else {
		return 0;
	}
}

void way(char **lab, int start_i, int start_j) {
	int i = start_i;
	int j = start_j;
	char quest = 0;
	char* sled = (char*)calloc(n*m, sizeof(char));
	int sup = 0;
	char check;
	char sost[4][2];
	sost[0][0] = -1;
	sost[0][1] = 0;//top
	sost[1][0] = 1;
	sost[1][1] = 0;//down
	sost[2][0] = 0;
	sost[2][1] = 1;//right
	sost[3][0] = 0;
	sost[3][1] = -1;//left
	while (lab[i][j] != 'B') {
		check = 0;
		for (int k = 1; k < 5; ++k) {
			if (proverka(lab, i + sost[k - 1][0], j + sost[k - 1][1])) {
				sled[sup] = k;
				check = 1;
				++sup;
			}
		}
		if (check == 0) {
			if (i == start_i && j == start_j) {
				printf("error way");
			}
			else {
				lab[i][j] = 'x';
				for (int k = 1; k < 5; ++k) {
					if (sled[sup - 1] == k) {
						sled[sup - 1] = 0;
						i -= sost[k - 1][0];
						j -= sost[k - 1][1];
						break;
					}
				}
				--sup;
			}
		}
		else {
			lab[i][j] = '*';
			for (int k = 1; k < 5; ++k) {
				if (sled[sup - 1] == k) {
					i += sost[k - 1][0];
					j += sost[k - 1][1];
					break;
				}
			}
		}
	}
	free(sled);
}

int main(int argc, char* argv[]) {
	if (argc != 2) {
		printf("error command arguments");
		return 0;
	}
	int i;
	int j = 0;
	int start_i;
	int start_j;
	int fsize;
	int proverka = 0;
	int jump = 1;
	char *mass = NULL;
	char **lab = NULL;
	FILE *myfile = NULL;
	myfile = fopen(argv[1], "rb");
	char temp = -3;
	if (myfile == NULL) {
		printf("ошибка чтения из файла");
		return 0;
	}
	fseek(myfile, 0, SEEK_END);
	fsize = ftell(myfile);
	if (fsize == 0) {
		printf("Файл пустой");
		return 0;
	}
	fseek(myfile, 0, SEEK_END);
	fsize = ftell(myfile);
	if (fsize == 0) {
		printf("Файл пустой");
		return 0;
	}
	mass = (char*)malloc(fsize*sizeof(char) + 1);
	fseek(myfile, 0, SEEK_SET);
	size_t kol = fread(mass, 1, fsize, myfile);
	mass[kol] = '\0';
	fclose(myfile);
	if (kol != fsize) {
		printf("Файл считался некорректно");
		return 0;
	}
	i = 0;
	while (mass[i] != '\0') {
		if (mass[i] == '\r') {
			++n;
			--m;
			++i;
		}
		++i;
		++m;
	}
	++n;
	m /= n;
	lab = (char**)malloc(n*sizeof(char*));
	lab[0] = mass;
	for (i = 1; i < n; ++i) {
		lab[i] = &mass[(m + 2) * jump];
		++jump;
	}
	while (lab[0][j] != '\r') {
		if (lab[0][j] == ' '  || lab[0][j] == '#' || lab[0][j] == 'B') {
			++proverka;
		}
		if (lab[0][j] == 'A') {
			start_i = 0;
			start_j = j;
			++proverka;
		}
		++j;
	}
	j = 0;
	for (i = 1; i < n; ++i) {
		while (lab[i][j] != '\r' && lab[i][j] != '\0') {
			if (lab[i][j] == 'A') {
				start_i = i;
				start_j = j;
			}
			++j;
		}
		if (j != proverka) {
			printf("Некорректно задан формат поля");
			return 0;
		}
		j = 0;
	}
	for (i = 0; i < n; ++i) {
		for (j = 0; j < m; ++j) {
			printf("%c", lab[i][j]);
		}
		printf("\n");
	}
	printf("\n");
	way(lab, start_i, start_j);
	lab[start_i][start_j] = 'A';
		for (i = 0; i < n; ++i) {
			for (j = 0; j < m; ++j) {
				if (lab[i][j] == 'x') {
					lab[i][j] = ' ';
				}
				printf("%c", lab[i][j]);
			}
			printf("\n");
		}
	free(lab);
	free(mass);
	return 0;
}