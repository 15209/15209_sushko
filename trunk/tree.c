﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

typedef struct spisok {
	struct  spisok *next;
	int numb;
} spisok;

void new(spisok** blok, int numb){
	spisok* ykaz = malloc(sizeof(spisok));
ykaz->numb = numb;
ykaz->next = *blok;
*blok = ykaz;
}

typedef struct tree {
	struct  tree *left;
	struct  tree *right;
	int numb;
} tree;

void sortirovka(spisok* first, int len) {
	spisok* dop;
	int i;
	int j;
	int temp;
	for (i = 0; i < len; ++i) {
		dop = first;
		for (j = 0; j < len - i - 1; ++j) {
			if (dop->numb > dop->next->numb) {
				temp = dop->numb;
				dop->numb = dop->next->numb;
				dop->next->numb = temp;
			}
			dop = dop->next;
		}
	}
}

tree *derevo(spisok **spk, int nb) {
	if (nb <= 0) {
		return NULL;
	}
	tree *left = derevo(spk, nb/2);
	tree *kor = (tree *)malloc(sizeof(tree));
	kor->left = left;
	kor->right = NULL;
	kor->numb = (*spk)->numb;
	(*spk) = (*spk)->next;
	kor->right = derevo(spk, nb-nb/2-1);
	return kor;
}

void print(tree *p, int glubina) {
	int inside = glubina + 1;
	if (p) {
		print(p->left, inside);
		for (int i = 0; i < glubina * 5; ++i) {
			printf(" ");
		}
		printf("%i\n", p->numb);
		print(p->right, inside);
	}
}

void free_memory(tree *first) {
	if (first->left != NULL) {
		free_memory(first->left);
	}
	if (first->right != NULL) {
		free_memory(first->right);
	}
	free(first);
}

int main(int argc, char *argv[]) {
	if (argc != 2) {
		printf("error command arguments");
		return 0;
	}
	int temp;
	int kol_vo = 0;
	int sch = 1;
	FILE* myfile = NULL;
	spisok* ykaz_first = NULL;
	spisok* fix_begin = NULL;
	spisok* support = NULL;
	tree *first = NULL;
	myfile = fopen(argv[1], "r");
	while (!feof(myfile)) {
		fscanf(myfile, "%i", &temp);
		new(&ykaz_first, temp);
		++kol_vo;
	}
	fix_begin = ykaz_first;
	sortirovka(ykaz_first, kol_vo);
	while (fix_begin->next != NULL) {
		support = fix_begin->next;
		if (fix_begin->numb == support->numb) {
			fix_begin->next = support->next;
			free(support);
			--kol_vo;
		}
		fix_begin = fix_begin->next;
	}
	fix_begin = ykaz_first;
	while (fix_begin) {
		printf("%i ", fix_begin->numb); 
		fix_begin = fix_begin->next;
	}
	printf("\n");
	fix_begin = ykaz_first;
	first = derevo(&fix_begin, kol_vo);
	while (fix_begin != NULL) {
		support = fix_begin;
		fix_begin = fix_begin->next;
		free(support);
	} 
	print(first, 0);
	free_memory(first);
	return 0;
}